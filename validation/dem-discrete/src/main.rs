// get the crates
extern crate prestige;


// local imports
use std::env;
use std::io;
use std::io::prelude::*;
use std::process::exit;



const USAGE: &'static str = "
Usage: dem-discrete bench
       dem-discrete <benchmark-name> [ options ]
       dem-discrete --help
A collection of different benchmarks of Discrete element method.
Alternatively, you can run individual benchmarks by running `dem-continuous foo`,
where `foo` is the name of a benchmark. Each benchmark has its own options and
modes, so try `dem-continuous foo --help`.

Benchmarks:
  - life : Conway's Game of Life.
  - nbody: A physics simulation of multiple bodies attracting and repelling
           one another.
  - sieve: Finding primes using a Sieve of Eratosthenes.
  - matmul: Parallel matrix multiplication.
  - mergesort: Parallel mergesort.
  - quicksort: Parallel quicksort.
  - tsp: Traveling salesman problem solver (sample data sets in `data/tsp`).
";

fn usage() -> ! {
    let _ = writeln!(&mut io::stderr(), "{}", USAGE);
    exit(1);
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        usage();
    }

    let bench_name = &args[1];
    match &bench_name[..] {
        "matmul" => matmul::main(&args[1..]),
        "mergesort" => mergesort::main(&args[1..]),
        "nbody" => nbody::main(&args[1..]),
        "quicksort" => quicksort::main(&args[1..]),
        "sieve" => sieve::main(&args[1..]),
        "tsp" => tsp::main(&args[1..]),
        "life" => life::main(&args[1..]),
        _ => usage(),
    }
}
